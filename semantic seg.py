# -*- coding: utf-8 -*-

# Copyright The PyTorch Lightning team.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from argparse import ArgumentParser, Namespace

import numpy as np
import pytorch_lightning as pl
import torch
import torch.nn.functional as F
import torchvision.transforms as T
from pl_examples.domain_templates.unet import UNet
from torch.utils.data import DataLoader
from torchvision.datasets.coco import CocoDetection
import coco_annotation as cca

DEFAULT_VOID_LABELS = (0, 1, 2, 3, 4, 5, 6, 9, 10, 14, 15, 16, 18, 29, 30, -1)
DEFAULT_VALID_LABELS = (7, 8, 11, 12, 13, 17, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 31, 32, 33)


# def _create_synth_kitti_dataset(path_dir: str, image_dims: tuple = (1024, 512)):
#     """Create synthetic dataset with random images, just to simulate that the dataset have been already downloaded."""
#     path_dir_images = os.path.join(path_dir, KITTI.IMAGE_PATH)
#     path_dir_masks = os.path.join(path_dir, KITTI.MASK_PATH)
#     for p_dir in (path_dir_images, path_dir_masks):
#         os.makedirs(p_dir, exist_ok=True)
#     for i in range(3):
#         path_img = os.path.join(path_dir_images, f"dummy_kitti_{i}.png")
#         Image.new("RGB", image_dims).save(path_img)
#         path_mask = os.path.join(path_dir_masks, f"dummy_kitti_{i}.png")
#         Image.new("L", image_dims).save(path_mask)

#coco défini (DONE)


class SegModel(pl.LightningModule):

    def __init__(
        self,
        batch_size: int = 4,
        lr: float = 1e-3,
        num_layers: int = 3,
        features_start: int = 64,
        bilinear: bool = False,
    ):
        super().__init__()
        self.batch_size = batch_size
        self.lr = lr
        self.num_layers = num_layers
        self.features_start = features_start
        self.bilinear = bilinear
        
        train_data_dir = 'D:/Documents/code/DetectRust/detection/input'
        train_coco = 'D:/Documents/code/DetectRust/detection/labelled.json'


        self.net = UNet(
            num_classes=13, num_layers=self.num_layers, features_start=self.features_start, bilinear=self.bilinear
        )
        self.transform = T.Compose(
            [
                T.Resize((64,64)),
                T.ToTensor(),
                T.Normalize(
                     mean=[0.35675976, 0.37380189, 0.3764753], std=[0.32064945, 0.32098866, 0.32325324]
                 ),
            ]
        )
        dataset = CocoDetection(root=train_data_dir,
                     annFile=train_coco, transform=self.transform)
        indices = torch.randperm(len(dataset)).tolist()
        self.trainset = torch.utils.data.Subset(dataset, indices[:-10])
        self.validset = torch.utils.data.Subset(dataset, indices[-10:])
        test = dataset.__getitem__(0)
    
    def forward(self, x):
        return self.net(x)

    def training_step(self, batch, batch_nb):
        img, mask = batch
     #   torch.tensor(mask, dtype=torch.float32)
        img = img.float()
        mask = mask.long()
        out = self(img)
        loss = F.cross_entropy(out, mask, ignore_index=250)
        log_dict = {"train_loss": loss}
        return {"loss": loss, "log": log_dict, "progress_bar": log_dict}

    def validation_step(self, batch, batch_idx):
        img,mask = batch
        torch.tensor(mask)
        img = img.float()
        mask = mask.long()
        out = self(img)
        loss_val = F.cross_entropy(out, mask, ignore_index=250)
        return {"val_loss": loss_val}

    def validation_epoch_end(self, outputs):
        loss_val = torch.stack([x["val_loss"] for x in outputs]).mean()
        log_dict = {"val_loss": loss_val}
        print("validation")
        return {"log": log_dict, "val_loss": log_dict["val_loss"], "progress_bar": log_dict}

    def configure_optimizers(self):
        opt = torch.optim.Adam(self.net.parameters(), lr=self.lr)
        sch = torch.optim.lr_scheduler.CosineAnnealingLR(opt, T_max=10)
        return [opt], [sch]

    def train_dataloader(self):
        return DataLoader(self.trainset, batch_size=self.batch_size, shuffle=True)

    def val_dataloader(self):
        return DataLoader(self.validset, batch_size=self.batch_size, shuffle=False)

    @staticmethod
    def add_model_specific_args(parent_parser):  # pragma: no-cover
        parser = parent_parser.add_argument_group("SegModel")
        parser.add_argument("--data_path", type=str, help="path where dataset is stored")
        parser.add_argument("--batch_size", type=int, default=16, help="size of the batches")
        parser.add_argument("--lr", type=float, default=0.001, help="adam: learning rate")
        parser.add_argument("--num_layers", type=int, default=5, help="number of layers on u-net")
        parser.add_argument("--features_start", type=float, default=64, help="number of features in first layer")
        parser.add_argument(
            "--bilinear", action="store_true", default=False, help="whether to use bilinear interpolation or transposed"
        )
        return parent_parser


def main(hparams: Namespace):
    # ------------------------
    # 1 INIT LIGHTNING MODEL
    # ------------------------
    model = SegModel()

    # ------------------------
    # 2 SET LOGGER
    # ------------------------
    logger = False

    # ------------------------
    # 3 INIT TRAINER
    # ------------------------
    trainer = pl.Trainer.from_argparse_args(hparams)
    
    # ------------------------
    # 5 START TRAINING
    # ------------------------
    trainer.fit(model)


if __name__ == "__main__":
    # cli_lightning_logo()
    parser = ArgumentParser(add_help=False)
    parser = SegModel.add_model_specific_args(parser)
    hparams = parser.parse_args()

    main(hparams)